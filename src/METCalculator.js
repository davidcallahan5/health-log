/**
 * Calculate MET while running
 * https://sites.google.com/site/compendiumofphysicalactivities/Activity-Categories/running
 * @param {Float} speed The average speed being ran at
 * @return {Float} The Metabolic Equivalent of Task (MET)
 */
function runningMET(speed) {
  let met = 0;
  if(speed >= 14) {
    met = 23.0;
  } else if(speed >= 13) {
    met = 19.8;
  } else if(speed >= 12) {
    met = 19.0;
  } else if(speed >= 11) {
    met = 16.0;
  } else if(speed >= 10) {
    met = 14.5;
  } else if(speed >= 9) {
    met = 12.8;
  } else if(speed >= 8) {
    met = 11.8;
  } else if(speed >= 7.5) {
    met = 11.8;
  } else if(speed >= 7) {
    met = 11.0;
  } else if(speed >= 6) {
    met = 9.8;
  } else if(speed >= 5) {
    met = 8.3;
  } else {
    met = 6.0;
  }

  return met;
}

/**
 * Calculate MET while walking
 * https://sites.google.com/site/compendiumofphysicalactivities/Activity-Categories/walking
 * @param {Float} speed The average speed being walked at
 * @return {Float} The Metabolic Equivalent of Task (MET)
 */
function walkingMET(speed) {
  let met = 0;
  if(speed >= 5) {
    met = 8.3;
  } else if(speed >= 4.5) {
    met = 7.0;
  } else if(speed >= 4) {
    met = 5.0;
  } else if(speed >= 3.5) {
    met = 4.3;
  } else if(speed >= 2.8) {
    met = 3.5;
  } else if(speed >= 2.5) {
    met = 3.0;
  } else {
    met = 3.0;
  }

  return met;
}

/**
 * Calculate MET for given exercise at given speed
 * @param {string} exercise The exercise to calculate the MET for
 * @param {Float} speed the average speed for the given exercise. Default=0
 * @return {Float} The Metabolic Equivalent of Task (MET)
 */
export function calculateMET(exercise, speed=0) {
  let met = 0;
  switch(exercise)
  {    
    case "running":
    {
      met = runningMET(speed);
      break;
    }    
    
    case "walking":
    {
      met = walkingMET(speed);
      break;
    }
    
    default:
    {
      console.log("Cannot give met for exercise " + exercise);
      met = 0;
      break;
    }
  }
  
  return met;
}