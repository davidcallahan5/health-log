import React from 'react';
import '../styles/Home.css';

import {Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';

/**
 * Basic Home page
 */
function Home() {
  return (
    <div className="Home">
      <header className="Home-header">
        <p>
          Health Log
        </p>        
      </header>
      <div className="Home-image-container">
        <Image
          className="Home-image"
          src={process.env.PUBLIC_URL + "/pexels-lukas-349608.jpg"}>
        </Image>
        <div className="Home-image-text-container">
          <header className="Home-image-text"> 
            Manage your health
          </header>
          <br />
          <Link to="./trackers" className="Home-continue-button">
            Continue
          </Link>
        </div>
      </div>      
    </div>
  );
}

export default Home;
