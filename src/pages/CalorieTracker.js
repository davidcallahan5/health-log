import React, { useEffect, useState, useRef } from 'react';

import {Link} from 'react-router-dom';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

import { displayDate, assignDate, checkDate} from '../DateHandler.js'

const api = `https://api.spoonacular.com/`;
const apiKey = `f365d57085904418839279d80ccc4e92`;
const number = `10`;                                // Limit number of pulled items to 10. Reduces account queries.
const maxCalories = `10000`;                        // Max calories needed or calories will not be pulled in nutrition array
const addRecipeNutrition = `true`;                  // Get number of servings

/**
 * Log calorie information by each day
 */
function CalorieTracker() {
  const didMountRef = useRef(false);
  const [selected, setDate] = useState(assignDate(new Date()));
  const [recipe, setRecipe] = useState([]);
  const [search, setSearch] = useState("");
  const [newSearch, setNewSearch] = useState("");
  const [options, setOptions] = useState([]);
  const [recipeLog, setRecipeLog] = useState([]);

  /**
   * GET ingredient information by query
   * https://spoonacular.com/food-api/docs#Search-Recipes-Complex
   */
  useEffect(() => {
    if(didMountRef.current) {
      fetch (
        api + `recipes/complexSearch?query=${search}&number=`+ number + `&maxCalories=` + maxCalories + `&addRecipeNutrition=` + addRecipeNutrition + `&apiKey=` + apiKey,
        {
          method: "GET",
          headers: new Headers({
            "Accept": "application/json",
            "Content-Type": "application/json"
          })
        }      
      )
        .then(result => result.json())
        .then(response => {
          console.log(response)
          handleOptions(response.results);
        })
        .catch(error => console.log(error))
      }
      else {
        didMountRef.current = true;
      }
    }, [search]);



  /**
   * Checks if selected date exists in the recipe log.
   * @return {Date} The date that was found or undefined if not found 
   */
  function foundDate() {
    return ( 
      recipeLog.find((log) => {
        return checkDate(log, selected);
      })
    );
  }

  /**
   * Adds new field to recipe log or updates an existing field. 
   */
  function assignRecipeLog(rec) {
    if(!foundDate()) {
      //Add new field
      setRecipe(rec);
      console.log(rec);
      setRecipeLog(recipeLog => [...recipeLog, {"date" : selected, "recipe" : rec}]);
    } else {
      //Update existing field
      setRecipeLog(recipeLog.map(log => 
        checkDate(log, selected) && log.recipe !== rec
          ? {...log, "recipe": rec}
          : log));
    }
  }

  /**
   * Handle updating of ingredientId.
   * Should not change if we don't have to since we want to make as few API calls as possible
   * @param {string} s recipe search criteria
   */
  function changeSearch() {
    if(newSearch !== search && newSearch !== "") {
      setSearch(newSearch);
    }
  }

  /**
   * Convert results into options that may be displayed in the searchbar
   * @param {Object[]} results Pulled results from GET request for searched recipe
   */
  function handleOptions(results) {
    results = results.slice(0, 10).map(res => (      
      setOptions(options => [...options, {id: res.id, title: res.title, calories: res.nutrition[0].amount, image: res.image, servings: res.servings}])
    ));
  }

  /**
   * Handles user keypress. Checks for Enter key.
   * Clear current options first
   * @param {KeyboardEvent} event The keyboard event 
   */
  function handleKeyPress(event) {
    if (event.key === "Enter") { 
      setOptions([]);
      changeSearch();
    }
  }

  /**
   * Displays search options
   * @TODO display images in a row to fit the screen then repeat on next line. Or 5 on first row 5 on second.
   * @TODO give selected image a border
   */
  function displayOptions() {
    return(
      options.map(i => (
        <div key={i.id}>
          <header>
            {i.title} <br />
            {i.calories} <br />
            {i.servings} <br />
          </header>
          <img
            alt={i.title}
            src={i.image}
            onClick={() => assignRecipeLog(i)}
          />
        </div>
      ))
    );
  }

  return (
    <div>
      <Link to="trackers">
        Back to Trackers Board
      </Link>

      <Calendar 
        maxDate={new Date()}
        onClickDay={d => setDate(assignDate(d))}
      /> <br />

      <input 
        value={newSearch}
        onChange={i => setNewSearch(i.target.value)}
        onKeyPress={e => handleKeyPress(e)}
        placeholder={"Enter recipe to log..."}/>      

      <p>
        <br />
        Date: {displayDate(selected)} <br />
        Recipe: {recipeLog.recipe} <br />
        Calories per serving: {recipe.calories} <br />
        Number of servings: {recipe.servings} <br />
      </p>
      <img 
        alt={recipe.title} 
        src={recipe.image}>
      </img> <br /> <br />

      <div>
        {displayOptions()}
      </div>
    </div>
  );
}

export default CalorieTracker;