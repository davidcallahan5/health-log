import React, {useState} from 'react';

import {Link} from 'react-router-dom';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

import { displayDate, assignDate, checkDate} from '../DateHandler.js'

/**
 * Log weight information by each day
 * * selected: the selected date on the calendar
 * * weight: the entered weight for the given day
 * * newWeight: the entered weight from the input field to be checked if it is a number
 * * weightLog: array of mapped weights to a given day
 * 
 * @TODO weight log initial array should pull persistent data (and save as well)
 * @TODO try catch block to handle disabled cookies for imported CSS (The operation is insecure)
 * @TODO graph data x: dates by each day, y: weight in pounds
 */
function WeightTracker() {
  const [selected, setDate] = useState(assignDate(new Date()));
  const [weight, setWeight] = useState(0);
  const [newWeight, setNewWeight] = useState(0);
  const [weightLog, setWeightLog] = useState([]);

  /**
   * Checks if selected date exists in the weight log.
   * @return {Date} The date that was found or undefined if not found 
   */
  function foundDate() {
    return ( 
      weightLog.find((log) => {
        return checkDate(log, selected);
      })
    );
  }

  /**
   * Adds new field to weight log or updates an existing field. 
   */
  function assignWeightLog() {
    if(!foundDate()) {
      //Add new field
      setWeight(newWeight);
      setWeightLog(weightLog => [...weightLog, {"date" : selected, "weight" : newWeight}]);
    } else {
      //Update existing field
      setWeightLog(weightLog.map(log => 
        checkDate(log, selected) && log.weight !== newWeight
          ? {...log, "weight": newWeight}
          : log));
    }
  }

  /**
   * Handles user keypress. Checks for Enter key.
   * @param {KeyboardEvent} event The keyboard event 
   */
  function handleKeyPress(event) {
    if (event.key === "Enter") { 
      handleButtonEnter();
    }
  }

  /**
   * Handles user button click.
   */
  function handleButtonEnter() {
    if(!isNaN(newWeight)) {
      assignWeightLog();
    }
    else {
      /**@TODO Add red border to input if new_weight is NaN*/
    }
  }

  /**
   * Clear data by selected date
   */
  function handleButtonClear() {
    if(foundDate()) {
      //Remove field by selected date
      setWeightLog(weightLog.filter(log => 
        !checkDate(log, selected)
      ))
    }
  }

  /**
   * Renders the weight log 
   * @return The weight log objects as a list
   */
  function renderWeightLog() {
    return (
      weightLog.map((log, index) => (
      <li key={index}>{log.weight} {displayDate(log.date)}</li>
    )));
  }

  return (
    <div>
      <Link to="trackers">
        Back to Trackers Board
      </Link>

      <Calendar 
        maxDate={new Date()}
        onClickDay={d => setDate(assignDate(d))}
      /> <br />

      <input
        type="text"
        value={newWeight}
        onChange={w => setNewWeight(w.target.value)}   
        onKeyPress={event => handleKeyPress(event)}     
      /> <br />

      <button onClick={() => handleButtonEnter()}>
        Enter
      </button> 
      
      <button onClick={() => handleButtonClear()}>
        Clear
      </button> <br />

      <p>
        Selected Date: {displayDate(selected)} <br />
        lbs: {weight} <br />
      </p>
      <ol>
        {renderWeightLog()}
      </ol>
    </div>
  );
}

export default WeightTracker;