import React from 'react';

import '../styles/Trackers.css'
import { Link } from 'react-router-dom';

/**
 * Board that lists health trackers.
 * 
 * user:
 * {
 *   id: 
 *   name:
 *   recipeLog:
 *   {
 *     date:
 *     recipe:
 *     {
 *       id:
 *       title:
 *       calories:
 *       image:
 *       servings:
 *     }
 *     totalCalories:
 *   }
 *   weightLog:
 *   {
 *     date:
 *     weight: 
 *   }
 *   exerciseLog:
 *   {
 *     date:
 *     exerciseLog:
 *     {
 *       exercise:
 *       caloriesBurned:
 *     }
 *     totalCaloriesBurned:
 *   }
 * }
 * 
 * @TODO update trackers value and value_units to pull information from their corresponding trackers
 */
function Trackers() {
  let trackers = [
    {
      "name": "Weight",
      "value": 190,
      "value_units": "lbs",
      "link": "weight-tracker"
    },
    {
      "name": "Calories",
      "value": 2000,
      "value_units": "calories",
      "link": "calorie-tracker"
    },
    {
      "name": "Exercises",
      "value": "Running",
      "value_units": "exercise",
      "link": "exercise-tracker"
    }];
  const ListTrackers = trackers.map((t) => (
        <div className="Tracker" key={t.name}>
          <header>
            {t.name}
          </header>
          <p>
            {t.value} {t.value_units}
          </p>
          <Link to={t.link}>
            Log {t.name}
          </Link>
        </div>
      ));
  return (
    <div>
        <Link to="/">
         Home
        </Link>
      <div className="Tracker-board">
        <div>
          {ListTrackers}
        </div>     
      </div>
    </div>
  );
}

export default Trackers;