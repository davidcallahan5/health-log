import React, {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';
import Calendar from 'react-calendar';

import { displayDate, assignDate, checkDate} from '../DateHandler.js'
import { calculateMET} from '../METCalculator.js'
/**
 * List of exercises for user to choose from
 */
const exercises = [
  {
    "name": "running"
  },
  {
    "name": "walking"
  },
  {
    "name": "weight training"
  }
];

/**
 * @TODO Replace with most recent entry in WeightTracker.js weight field
 */
const bodyMassKg = 86.1826; //190lb / 86.1826kg

/**
 * Log exercise information by each day
 */
function ExerciseTracker() {
  const [selected, setDate] = useState(assignDate(new Date()));
  const [calories, setCaloriesBurned] = useState(0);
  const [exercise, setExercise] = useState("running");
  const [exerciseLog, setExerciseLog] = useState([]);
  const [minutes, setMinutes] = useState("");
  const [speed, setSpeed] = useState("");

  /**
   * Ensures exercise log is updated when asynchronous setCalories is called
   */
  useEffect(() => {
    assignExerciseLog();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [calories]);


  /**
   * List the exercise options from the exercises array
   */
  function listExerciseOptions() {
    return (exercises.map(e => (
      <option key={e.name}>{e.name}</option>
    )));
  }

  /**
   * Using the populated exercise type and time, calculate calories burned.
   * Other information pulled externally: age, weight for MET
   * kcal ~= MET * kg * h (from https://en.wikipedia.org/wiki/Metabolic_equivalent_of_task)
   * * kcal: kilocalorie
   * * MET:  Metabolic Equivalent of Task
   * * kg:   weight in kilograms
   * * h:    time in hours
   */
  function calculateCalories() {
    let met = 0;

    if(minutes > 0 && speed > 0) {
      met = calculateMET(exercise, speed);
      let cals = met * bodyMassKg * (minutes/60);
      setCaloriesBurned(cals);
      console.log(calories);
      console.log(met * bodyMassKg * (minutes/60));
      return true;
    } else {
      //@TODO highlight input field red with missing input values
      console.log("bad entry");
      return false;
    }
  }

  /**
   * Checks if selected date exists in the exercise log.
   * @return {Date} The date that was found or undefined if not found 
   */
  function foundDate() {
    return ( 
      exerciseLog.find((log) => {
        return checkDate(log, selected);
      })
    );
  }  

  /**
   * Renders the exercise log 
   * @return The exercise log objects as a list
   */
  function renderExerciseLog() {
    return (
      exerciseLog.map((log, index) => (
      <li key={index}>{log.exercise} {log.calories} {displayDate(log.date)}</li>
    )));
  }

  /**
   * Clear data by selected date
   */
  function handleButtonClear() {
    if(foundDate()) {
      //Remove field by selected date
      setExerciseLog(exerciseLog.filter(log => 
        !checkDate(log, selected)
      ))
    }
  }

  /**
   * Adds new field to exercise log or updates an existing field. 
   */
  function assignExerciseLog() {
    if(calculateCalories()) {
      if(!foundDate()) {
        //Add new field
        setExerciseLog(exerciseLog => [...exerciseLog, {"date" : selected, "exercise" : exercise, "calories" : calories}]);
      } else {
        //Update existing field
        setExerciseLog(exerciseLog.map(log => 
          checkDate(log, selected) && (log.exercise !== exercise || log.calories !== calories)
            ? {...log, "exercise": exercise, "calories": calories}
            : log));
      }
    }
  }

  return (
    <div>
      <Link to="trackers">
        Back to Trackers Board
      </Link>

      <Calendar 
        maxDate={new Date()}
        onClickDay={d => setDate(assignDate(d))}
        />

      <select
        onChange={item => setExercise(item.target.value)      
        }>
        {listExerciseOptions()}
      </select> <br />

      <input 
        placeholder="Enter total minutes..." 
        value={minutes}
        onChange={min => setMinutes(min.target.value)}/> <br />
      
      <input 
        placeholder="Enter average speed..."
        value={speed}
        onChange={spd => setSpeed(spd.target.value)}/> <br />

      <button
        onClick={() => assignExerciseLog()}>
        Log Exercise
      </button>

      <button
        onClick={() => handleButtonClear()}>
        Clear Log
      </button>

      <p>
        {displayDate(selected)} <br />
        Calories Burned: {calories} <br />
        Minutes during {exercise} exercise: {minutes}
      </p>
      <div>{renderExerciseLog()}</div>
    </div>
  );
}

export default ExerciseTracker;