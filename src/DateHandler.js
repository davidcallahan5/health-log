/**
 * Display the given date as a string
 * @param {Date} date The date to be displayed
 * @TODO customize to display off of user preferences MM/dd/YYYY dd/MM/YYYY and so on
 */
export function displayDate(date) {
  return (date.getMonth()+1).toString() + "/" + date.getDate().toString() + "/" + date.getFullYear().toString();
}

/**
 * Creates a new Date that only has the month date and year
 * @param {Date} date The date to be reduced 
 * @return {Date} The date with the month date and year
 */
export function assignDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

/**
 * Checks selected date only by date, month, and year
 * @param {Date} log Other dates
 * @param {Date} sel The selected date
 * @return true if equivalent; false otherwise
 */
export function checkDate(log, sel) {
  return (
    log.date.getDate() === sel.getDate() 
        && log.date.getMonth() === sel.getMonth() 
        && log.date.getFullYear() === sel.getFullYear()
  );
}