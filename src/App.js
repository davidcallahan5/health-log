import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import Home from './pages/Home';
import Trackers from './pages/Trackers';
import WeightTracker from './pages/WeightTracker';
import CalorieTracker from './pages/CalorieTracker';
import ExerciseTracker from './pages/ExerciseTracker';

/**
 * Switches between various pages.
 */
function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route path="/trackers" component={Trackers}></Route>
            <Route path="/weight-tracker" component={WeightTracker}></Route>
            <Route path="/calorie-tracker" component={CalorieTracker}></Route>
            <Route path="/exercise-tracker" component={ExerciseTracker}></Route>
          </Switch>
        </div>
      </Router>    
    </div>
  );
}

export default App;
